function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
      <div class="card mb-4 shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-subtitle mb-2 text-muted">${location}</p>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">${starts} - ${ends}</div>
      </div>
    `;
  }

function sendAlert(message) {
  return `<div class="alert alert-danger align-items-center" role="alert">${message}</div>`;
  }


  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        alert.innerHTML = sendAlert("Cannot find list of conferences");
      } else {
        const data = await response.json();

        const columns = document.querySelectorAll('.col');
        let i = 0;

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const start = (new Date(details.conference.starts)).toLocaleDateString();
            const end = (new Date(details.conference.ends)).toLocaleDateString();
            const location = details.conference.location.name;
            const html = createCard(title, description, pictureUrl, start, end, location);
            columns[i % 3].innerHTML += html;
            i++;
          }
        }

      }
    } catch (e) {
      console.log(e);
    }

  });
